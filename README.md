Role Name
=========

A dumbed down version of [hispanico.nginx_revproxy](https://github.com/hispanico/ansible-nginx-revproxy).

Requirements
------------

Ansible 2.4 or higher.

Role Variables
--------------

Most of the variables available in 
[hispanico's role](https://github.com/hispanico/ansible-nginx-revproxy)
work in this role but with minor differences. Most notably, letsencrypt
and http password protection are removed. I'll probably add this
feature back at some point.

The only significant addition to the role is support for 
fetching the certificate from other server. It is meant
to be used with other roles in 
[this gitlab group](https://gitlab.com/galvesband-ansible),
like `certificate_authority` and `ca_based_certificate`.

```yml
reverse_proxy_sites:                                          # List of sites to reverse proxy
  default:                                                    # Set default site to return 444 (Connection Closed Without Response)
    ssl: false                                                # Set to True if you want to redirect http to https

  example.com:                                                # Domain name
    domains:                                                  # List of server_name aliases
      - example.com
      - www.example.com
    upstreams:                                                # List of Upstreams
      - { backend_address: 192.168.0.100, backend_port: 80 }
      - { backend_address: 192.168.0.101, backend_port: 8080 }
    listen: 9000                                              # Specify which port you want to listen to with clear HTTP, or leave undefined for 80
    ssl: false                                                # Set to True if you want to redirect http to https
    conn_upgrade: true                                        # Set the Connection upgrade header values

  example.org:                                                # Domain name
    domains:                                                  # List of server_name aliases
      - example.org
      - www.example.org
    upstreams:                                                # List of Upstreams
      - { backend_address: 192.168.0.200, backend_port: 80 }
      - { backend_address: 192.168.0.201, backend_port: 8080 }
    listen: 9000                                              # Specify which port you want to listen to with clear HTTP, or leave undefined for 80
    listen_ssl: 9001                                          # Specify which port you want to listen to with HTTPS, or leave undefined for 443
    ssl: true                                                 # Set to True if you want to redirect http to https
    ssl_fetch_cert: no                                        # Fetch the certificate from other server
    ssl_fetch_cert_host: localhost                            # Server where the custom certificate is
    ssl_fetch_cert_path: /srv/certs/example.org/cert.crt      # Location of public cert in remote server
    ssl_fetch_cert_key_path: /srv/certs/example.org/cert.key  # Location of private key in remote server
    ssl_certificate: /etc/ssl/certs/ssl-cert-snakeoil.pem     # ssl certificate, used if ssl_fetch_cert is no
    ssl_certificate_key: /etc/ssl/private/ssl-cert-snakeoil.key # ssl certificate key, used if ssl_fetch_cert is no
```


Dependencies
------------

None.

Example Playbook
----------------

```yml
 - hosts: proxy-server
   vars:
     reverse_proxy_sites:
       example.com:
         domains:
           - example.com
         upstream:
           - { backend_address: 10.45.1.3, backend_port: 8000 }
         ssl: true
         ssl_certificate: /srv/certs/example.com/cert.crt
         ssl_certificate_key: /srv/certs/example.com/cert.key
        
   roles:
     - reverse_proxy
```

License
-------

Licensed under the [GPLv3](https://spdx.org/licenses/GPL-3.0-only.html) License.

Author Information
------------------

This is a derived work from the more featured 
[hispanico.nginx_revproxy](https://github.com/hispanico/ansible-nginx-revproxy) role.
